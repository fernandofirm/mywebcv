import React, { Component } from 'react'
import { Layout, Icon , Row, Col , Carousel, Progress  } from 'antd'


import styled from 'styled-components'
// import Header from './components/Header'
// import Footer from './components/Footer'

import HeaderDesk from './components/HeaderDesk'
import About from './components/About'
import Skill from './components/Skill'
import Exp from './components/Exp'

import Button from './components/Button'

import mainPhoto from './img/mainPhoto.jpg'
import groupOfMe from './img/groupOfMe.jpg'
import profileFb from './img/profilefb.jpg'
import slide3 from './img/slide3.jpg'
import slide4 from './img/slide4.jpg' 

import Clock from './components/Clock'
import Contact from './components/Contact'


const { Header ,Content, Footer, Sider } = Layout;



class App extends Component {

  constructor(){
    super()

    this.state = {      
      firstName:  'Nititep',
      lastName: 'Chareonbamrung',
      sub: ['Student' , 'Creative', 'Programmer'],
      progress: 0,
      technology: false,
      technical: false,
      expdetect: false,
      htmlCss: 0,
      javascript: 0,
      jquery: 0,
      react: 0,
      python: 0,
      git: 0,
      firebase: 0,
      mysql: 0,      
      wordpress: 0,
      nodejs: 0,
      photoshop: 0,
      illustrator: 0,
      _3dsmax: 0,      
      problem: 0,
      creative: 0,
      critical: 0,
      googling: 0,
      marketing: 0,
      listening: 0,
      swot: 0,
      writing: 0,
      communicate: 0,
      
      }
  }
 
  componentDidMount () {
    // setInterval( () => this.increaseProgress , 5000)
    window.addEventListener('scroll', this.handleScroll );
   
  }

  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll );
  }


  render() {
    const { firstName, lastName , sub } = this.state  

    
    return (
     
      <Wrapper>        
        <Clock/>
        <HeaderDesk firstName={firstName} lastName={lastName}/>
        <Row>
        <Col span={2}></Col>
        <Col span={19} offset={1}>
        <Carousel autoplay className="ant-slide">
        <div><img src={mainPhoto} className="main-img"/></div>
        <div><img src={groupOfMe} className="group-img"/></div>
        <div><img src={slide3} className="group-img"/></div>
        <div><img src={slide4} className="group-img"/></div>
        </Carousel>        
        </Col>
        <Col span={2}></Col>
        </Row >        
        <About profileFb={profileFb}/>
        <Skill />
        <Exp />
        <Contact />            
      </Wrapper>
     
    );
  }
}

const Wrapper = styled.div`   
  background-color: #141414;
  
  .main-img {   
    width: 100%;
    background-size: cover;     
  }
  .group-img {
    width: 100%;
    backgroud-size: cover;
  }
  .group-img {
    width: 100%;
    backgroud-size: cover;
  }
  .ant-slide{     
    height: 50%;    
    background: #364d79;
    overflow: hidden;
    margin-right: auto;
    margin-left: auto;
    margin-top: 50px;
  }

 
`;

export default App;
