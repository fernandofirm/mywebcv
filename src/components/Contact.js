import React from 'react'
import styled from 'styled-components'
import { Layout , Input, Button} from 'antd'

const { TextArea } = Input;


const { Content } = Layout


const Contact = () => {
    return <Wrapper>
         <Content className="contact">
        <h1>Contact</h1><hr className="hr-line"/>
        <div className="flex-input">
        <a target="_blank" href="https://www.facebook.com/firm.nititep">
        <p className="contact-detail" >

        FB: Nititep Firm Chareonbamrung
        </p>

        </a>
        <p className="contact-detail">Line: firmeiei</p>
        <p className="contact-detail">E-mail: nititepc@gmail.com</p>
        <p className="contact-detail">Tel. 082-5487134</p>
        <Input placeholder="Name"  className="input-contact" />
        <Input placeholder="E-mail" className="input-contact" />
        <Input placeholder="Subject" className="input-contact" />
        <Input placeholder="Message" className="text-area" />
        <Button type="primary" className="contact-btn">Submit</Button>
        </div>
        
        </Content>
    </Wrapper>
    
}

const Wrapper = styled.div`

.contact {    
    height: 700px;
    background-color: #141414;
    h1 {      
      text-align: center;
      color: #72e3ff;
      padding-top: 50px;
    }
    .hr-line{
      width: 200px;
      margin-bottom: 50px;
    }

    .circle{
      width: 30px;
      height: 30px;
      border-radius: 30px;
      background-color: white;
      margin: 0 auto;     
      
    }

    .line { 
      width: 5px;
      height: 200px;;   
      background-color: #ffffff;
      margin: 0 auto;  
    }

    .flex-input {
        display: flex;
        flex-direction: column;
        
    }

    .input-contact {
        width: 500px;
        margin-right: auto;
        margin-left: auto;
        margin-bottom: 20px;       
        
    }

    .text-area {
        width: 500px;
        margin: auto;
        height: 100px;
    }

    .contact-btn {
        width: 300px;
        margin-top: 20px;
        margin-left: auto;
        margin-right: auto;
    }

    .contact-detail {
        text-align: center;
        color: #72e3ff;
        font-size: 20px;
    }

`

export default Contact