import React, { Component } from 'react';
import Responsive from 'react-responsive';
import 'antd/dist/antd.css';
import { Layout, Menu, Breadcrumb, Icon , Row, Col , Carousel  } from 'antd';


import styled from 'styled-components'

import MenuBar from './MenuBar'
import SlideShow from './SlideShow'


const Mobile = props => <Responsive {...props} maxWidth={767} />;


const { Header ,Content, Footer, Sider } = Layout;

const MobileRes = ({firstName, lastName, mainPhoto, groupOfMe, profileFb}) => {
    return (<Mobile>
        <MobileWrap>
        <h1 className="title">{firstName}</h1>
        <h1 className="title">{lastName}</h1>
        <h3 className="sub">Student , Creative, Programmer, Developer</h3>

        <MenuBar />
        <Row>
        <Col span={2}></Col>
        <Col span={19} offset={1}>
        <SlideShow mainPhoto={mainPhoto} groupOfMe={groupOfMe}/>    
        </Col>
        <Col span={2}></Col>
        </Row>
        <Content className="content1"><h1>About Me</h1><hr className="hr-line"/>
        <img src={profileFb} className="profilefb"/>       
        </Content> 
        </MobileWrap>
        </Mobile>)
}

const MobileWrap = styled.div`
background-color: #141414;
.title {
  text-align: center;
  color: #72e3ff;
  font-size: 30px;
}
.sub {
  color: #72e3ff;
  text-align: center;
}
.navbar {
  
  background-color: #141414;
  color: #72e3ff;
}

.main-img {   
  width: 100%;
  background-size: cover;   

}
.group-img {
  width: 100%;
  backgroud-size: cover;
}
.ant-slide{ 
  margin-top: 50px;     
  background: #364d79;
  overflow: hidden;
}

.content1 {
    margin-top: 100px;
    height: 400px;
    background-color: #141414;
    h1 {
      
      text-align: center;
      color: #72e3ff;
      padding-top: 50px;
    }
    .hr-line{
      width: 200px;
      margin-bottom: 50px;
    }
    .profilefb{
      width: 200px;
      border-radius: 200px;
      display: block;      
      margin: 0 auto;

    }    

`


export default MobileRes