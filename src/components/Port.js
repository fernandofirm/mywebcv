import React from 'react'
import styled from 'styled-components'
import { Col, Row , Layout} from 'antd'

import mainPhoto from '../img/mainPhoto.jpg'


import HeaderDesk from './HeaderDesk'

const { Content } = Layout

const Port = () => {
    return <Wrapper>
        <HeaderDesk/>        
        <Content>
            <div>
                <div className="parallax">
                </div>  
            </div>          
        </Content>
    </Wrapper>
}

const Wrapper = styled.div`
height: 1000px;
.img-div {
    
}
.parallax {  
    margin-top: 50px;      
    background-image: url(${mainPhoto});
    height: 800px;
    /* Create the parallax scrolling effect */           
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-position: center;
}
`

// background-attachment: fixed; 

export default Port