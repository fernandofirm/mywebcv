import React from 'react'

import { Row, Col, Progress, Layout} from 'antd'
import styled from 'styled-components';

const { Content } = Layout

const Skill = () => {

    const uiDataTechno = ['Html & Css',
    'Javascript',
    'Vue',
    'React',
    'Python',
    'Git',
    'Firebase',
    'Mysql',
    'Nodejs',
    'Wordpress',
    'Photoshop',
    'Iilustrator',
    '3Ds Max']

    const uiDataTechni = [
      'Problem Solving',
      'Creative',
      'Critical Thinking',
      'Googling',
      'Digital Marketing',
      'Listening',
      'SWOT analysis',
      'Writing',
      'Communicate']


    return (<Wrapper>
        <Content className="skill"><h1>Skill</h1><hr className="hr-line"/>
        <h1>Technology</h1>
        <Row >
        <Col span={6} offset={0}>

            {uiDataTechno.map( (v, i )=> {
              return <h2 className="technology" key={v+i}>{v}</h2>
            })            
            }
        </Col>      
        <Col span={15} offset={1}>
        <Progress className="progress" percent={ 85 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 70 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 70 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 65 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 40 } showInfo={false} status="active" /> 
            <p></p>  <Progress className="progress" percent={ 70 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 60 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 70 } showInfo={false} status="active" /> 
            <p></p>  <Progress className="progress" percent={ 70 } showInfo={false} status="active" />        
            <p></p>  <Progress className="progress" percent={ 60 } showInfo={false} status="active" />  
            <p></p> <Progress className="progress" percent={ 70 } showInfo={false} status="active" />
            <p></p>  <Progress className="progress" percent={ 60 } showInfo={false} status="active" /> 
            <p></p> <Progress className="progress" percent={ 40 } showInfo={false} status="active" /> 
              <p></p>     

        </Col>
        <Col span={2}></Col>      
        </Row>
        <h1>Technical</h1>
        <Row >
        <Col span={6} offset={0}>
        

        {uiDataTechni.map( (v, i )=> {
          return <h2 className="technical" key={v+i}>{v}</h2>
        })            
        }
                
        </Col>      
        <Col span={15} offset={1}>
        <Progress className="progress" percent={ 70  } showInfo={false} status="active" />
        <p></p> 
        <Progress className="progress" percent={ 70 } showInfo={false} status="active" />
        <p></p> 
        <Progress className="progress" percent={ 60 } showInfo={false} status="active" />
        <p></p> 
        <Progress className="progress" percent={ 80 } showInfo={false} status="active" />
        <p></p> 
        <Progress className="progress" percent={ 60 } showInfo={false} status="active" /> 
        <p></p> 
        <Progress className="progress" percent={ 70 } showInfo={false} status="active" />
        <p></p> 
        <Progress className="progress" percent={ 60 } showInfo={false} status="active" />
        <p></p> 
        <Progress className="progress" percent={ 70 } showInfo={false} status="active" /> 
        <p></p> 
        <Progress className="progress" percent={ 50 } showInfo={false} status="active" />        
        <p></p> 
        
        </Col>
        <Col span={2}></Col>      
        </Row>
        </Content>

    </Wrapper>)
}

const Wrapper = styled.div`

.skill {
  margin-top: 100px;
  height: 1000px;
  background-color: #141414;
  h1 {      
    text-align: center;
    color: #72e3ff;
   
  }
  .hr-line{
    width: 200px;
    margin-bottom: 50px;
  }

  .technology {
    font-size: 17.5px;
    color: white;
    text-align: center;
  }
  
  .technical {
    font-size: 17.5px;
    color:white;
    text-align: center;
  } 

`

export default Skill