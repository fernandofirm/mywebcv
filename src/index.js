import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import 'antd/dist/antd.css';
import App from './App';
import Port from './components/Port'
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
<Router>   
<Switch>
  <Route exact path='/' component={App}/>
  <Route path='/port' component={Port}/>  
</Switch>
</Router>
, document.getElementById('root'));
registerServiceWorker();
